# App example

Abrir a aplicação e clicar play no arquivo CentralrelacionalmentoApplication

## Environments

| Environment | URL                |
|-------------|--------------------|
| localhost   | localhost:8080/api |

## Endpoints cURLs

### Cadastrar solicitação

curl --request POST \
--url http://localhost:8080/api/solicitacoes-clientes/cadastrar \
--header 'Content-Type: application/json' \
--cookie JSESSIONID=28925D66BE99D6A91587D75687195E75 \
--data '{
"cliente":"teste",
"mensagem":"erro no meu pagamento",
"equipeSolicitacao":"CARTOES"
}
'

### Listar todas solicitações

curl --request GET \
--url http://localhost:8080/api/solicitacoes-clientes/CARTOES \
--header 'Content-Type: application/json' \


##### Finalizar solicitações:
curl --request POST \
--url http://localhost:8080/api/solicitacoes-clientes/finalizar/9089e9d7-b1a8-4568-b2b2-7819b9aaca58 \
--header 'Content-Type: application/json' \
--cookie JSESSIONID=28925D66BE99D6A91587D75687195E75 \
--data '{
"equipeSolicitacao":"CARTOES"
}
'
package com.invext.centralrelacionalmento.domain.service;

import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import com.invext.centralrelacionalmento.service.AtendimentoService;
import com.invext.centralrelacionalmento.service.equipe.Equipe;
import com.invext.centralrelacionalmento.exception.AtendimentoNaoEncontradoException;
import com.invext.centralrelacionalmento.exception.NenhumAtendenteDisponivelException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AtendimentoServiceTest {

    @Mock
    private Equipe equipe;

    @InjectMocks
    private AtendimentoService atendimentoService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testBuscaAtendenteLivreComAtendenteDisponivel() throws NenhumAtendenteDisponivelException {
        Atendente atendente = mock(Atendente.class);
        List<Solicitacao> atendimentos = new ArrayList<>();
        atendimentos.add(new Solicitacao("teste","123456", EquipeSolicitacao.CARTOES));
        when(equipe.getAtendimentos()).thenReturn(new HashMap<Atendente, List<Solicitacao>>() {{
            put(atendente, atendimentos);
        }});

        assertEquals(atendente, atendimentoService.buscaAtendenteLivre(equipe));
    }

    @Test(expected = NenhumAtendenteDisponivelException.class)
    public void testBuscaAtendenteLivreSemAtendenteDisponivel() throws NenhumAtendenteDisponivelException {
        when(equipe.getAtendimentos()).thenReturn(new HashMap<>());
        atendimentoService.buscaAtendenteLivre(equipe);
    }

    @Test
    public void testEncaminhaAtendimento() throws NenhumAtendenteDisponivelException {
        Solicitacao solicitacao = new Solicitacao("teste","123456", EquipeSolicitacao.CARTOES);
        Atendente atendente = mock(Atendente.class);

        when(equipe.getAtendimentos()).thenReturn(new HashMap<Atendente, List<Solicitacao>>() {{
            put(atendente, new ArrayList<>());
        }});

        atendimentoService.encaminhaAtendimento(equipe, solicitacao);

        assertTrue(equipe.getAtendimentos().get(atendente).contains(solicitacao));
    }

    @Test
    public void testFinalizaAtendimento() throws AtendimentoNaoEncontradoException {
        UUID uuid = UUID.randomUUID();
        Solicitacao solicitacao = new Solicitacao("teste","123456", EquipeSolicitacao.CARTOES);
        solicitacao.setUuid(uuid);
        Atendente atendente = mock(Atendente.class);

        when(equipe.getAtendimentos()).thenReturn(new HashMap<Atendente, List<Solicitacao>>() {{
            put(atendente, new ArrayList<Solicitacao>() {{
                add(solicitacao);
            }});
        }});

        assertTrue(atendimentoService.finalizaAtendimento(equipe, uuid));
        assertTrue(equipe.getAtendimentos().get(atendente).isEmpty());
    }

    @Test(expected = AtendimentoNaoEncontradoException.class)
    public void testFinalizaAtendimentoNaoEncontrado() throws AtendimentoNaoEncontradoException {
        UUID uuid = UUID.randomUUID();
        when(equipe.getAtendimentos()).thenReturn(new HashMap<>());
        atendimentoService.finalizaAtendimento(equipe, uuid);
    }

    @Test
    public void testListaAtendimentos() {
        HashMap<Atendente, List<Solicitacao>> expected = new HashMap<>();
        when(equipe.getAtendimentos()).thenReturn(expected);

        assertEquals(expected, atendimentoService.listaAtendimentos(equipe));
    }
}
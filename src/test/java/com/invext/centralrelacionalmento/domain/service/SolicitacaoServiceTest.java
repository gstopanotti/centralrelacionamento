package com.invext.centralrelacionalmento.domain.service;

import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import com.invext.centralrelacionalmento.service.AtendimentoService;
import com.invext.centralrelacionalmento.service.SolicitacaoService;
import com.invext.centralrelacionalmento.service.equipe.Equipe;
import com.invext.centralrelacionalmento.exception.AtendimentoNaoEncontradoException;
import com.invext.centralrelacionalmento.exception.NenhumAtendenteDisponivelException;
import com.invext.centralrelacionalmento.factory.EquipeFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class SolicitacaoServiceTest {

    @Mock
    private EquipeFactory equipeFactory;

    @Mock
    private AtendimentoService atendimentoService;

    @InjectMocks
    private SolicitacaoService solicitacaoService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testaRegistrarSolicitacao() {
        Solicitacao solicitacao = new Solicitacao("mensagem", "cliente", EquipeSolicitacao.CARTOES);
        solicitacao.setUuid(UUID.randomUUID());
        Solicitacao result = solicitacaoService.registrarSolicitacao(solicitacao);

        assertEquals(solicitacao, result);
        assertTrue(solicitacaoService.solicitacaoQueue.contains(solicitacao));
    }

    @Test
    public void testaBuscaSolicitacaoList() {
        EquipeSolicitacao equipeSolicitacao = EquipeSolicitacao.CARTOES;
        Equipe equipe = mock(Equipe.class);
        when(equipeFactory.getAtendimentoByEquipe(equipeSolicitacao)).thenReturn(equipe);

        Solicitacao solicitacao = new Solicitacao("mensagem", "cliente", EquipeSolicitacao.CARTOES);
        HashMap<Atendente, List<Solicitacao>> expectedResult = (HashMap<Atendente, List<Solicitacao>>) new HashMap<>().put(new Atendente(1L, "teste"),
                Arrays.asList(solicitacao));

        when(atendimentoService.listaAtendimentos(equipe)).thenReturn(expectedResult);

        HashMap<Atendente, List<Solicitacao>> result = solicitacaoService.buscaSolicitacaoList(equipeSolicitacao);

        assertEquals(expectedResult, result);
    }

    @Test
    public void testaEncaminharSolicitacao() throws NenhumAtendenteDisponivelException {
        Solicitacao solicitacao = new Solicitacao("teste","123456", EquipeSolicitacao.CARTOES);
        solicitacao.setUuid(UUID.randomUUID());

        Equipe equipe = mock(Equipe.class);
        when(equipeFactory.getAtendimentoByEquipe(solicitacao.getEquipeSolicitacao())).thenReturn(equipe);


        solicitacaoService.registrarSolicitacao(solicitacao);
        solicitacaoService.encaminhaSolicitacao();

        assertTrue(solicitacaoService.solicitacaoQueue.isEmpty());
        verify(atendimentoService, times(1)).encaminhaAtendimento(equipe, solicitacao);
    }

    @Test
    public void testaFinalizarSolicitacao() throws AtendimentoNaoEncontradoException {
        Solicitacao solicitacao = new Solicitacao("teste","123456", EquipeSolicitacao.CARTOES);
        solicitacao.setUuid(UUID.randomUUID());

        Equipe equipe = mock(Equipe.class);
        when(equipeFactory.getAtendimentoByEquipe(solicitacao.getEquipeSolicitacao())).thenReturn(equipe);

        solicitacaoService.finalizarSolicitacao(solicitacao.getUuid(), solicitacao);

        verify(atendimentoService, times(1)).finalizaAtendimento(equipe, solicitacao.getUuid());
    }

//    @Test(expected = AtendimentoNaoEncontradoException.class)
//    public void testSolicitacaoNaoEncontrada()  {
//        Solicitacao solicitacao = new Solicitacao("teste","123456", EquipeSolicitacao.CARTOES);
//        solicitacao.setUuid(UUID.randomUUID());
//
//        Equipe equipe = mock(Equipe.class);
//        when(equipeFactory.getAtendimentoByEquipe(solicitacao.getEquipeSolicitacao())).thenReturn(equipe);
//
//        solicitacaoService.finalizarSolicitacao(solicitacao.getUuid(), solicitacao);
//
//    }
}
package com.invext.centralrelacionalmento.factory;

import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import com.invext.centralrelacionalmento.service.equipe.EquipeCartaoService;
import com.invext.centralrelacionalmento.service.equipe.EquipeEmprestimosService;
import com.invext.centralrelacionalmento.service.equipe.EquipeOutroService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;

public class EquipeFactoryTest {

    @Mock
    private EquipeCartaoService equipeCartaoService;

    @Mock
    private EquipeEmprestimosService equipeEmprestimosService;

    @Mock
    private EquipeOutroService equipeOutroService;

    @InjectMocks
    private EquipeFactory equipeFactory;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAtendimentoByEquipeCartoes() {
        assertEquals(equipeCartaoService, equipeFactory.getAtendimentoByEquipe(EquipeSolicitacao.CARTOES));
    }

    @Test
    public void testGetAtendimentoByEquipeEmprestimos() {
        assertEquals(equipeEmprestimosService, equipeFactory.getAtendimentoByEquipe(EquipeSolicitacao.EMPRESTIMOS));
    }

    @Test
    public void testGetAtendimentoByEquipeOutros() {
        assertEquals(equipeOutroService, equipeFactory.getAtendimentoByEquipe(EquipeSolicitacao.OUTROS));
    }
}
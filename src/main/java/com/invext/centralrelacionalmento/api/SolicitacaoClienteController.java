package com.invext.centralrelacionalmento.api;

import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import com.invext.centralrelacionalmento.service.SolicitacaoService;
import com.invext.centralrelacionalmento.dto.SolicitacaoClienteDTO;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping(value = "/solicitacoes-clientes")
public class SolicitacaoClienteController {

    static Logger logger = Logger.getLogger(SolicitacaoClienteController.class.getName());

    @Autowired
    SolicitacaoService solicitacaoService;

    @PostMapping(value="/cadastrar", produces = "application/json")
    public ResponseEntity<?> enviarSolicitacao(@RequestBody SolicitacaoClienteDTO solicitacaoDTO, HttpServletRequest request) {
        try {
           Solicitacao solicitacao = solicitacaoService.registrarSolicitacao(solicitacaoDTO.toDomain());
            return ResponseEntity.status(HttpStatus.CREATED)
                    .body(solicitacao.getUuid());
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        }
    }

    @PostMapping(value="/finalizar/{uuid}", produces = "application/json")
    public ResponseEntity<?> finalizarAtendimento(@PathVariable UUID uuid, @RequestBody SolicitacaoClienteDTO solicitacaoDTO, HttpServletRequest request) {
        try {
            solicitacaoService.finalizarSolicitacao(uuid, solicitacaoDTO.toDomain());
            return ResponseEntity.status(HttpStatus.OK)
                    .body(solicitacaoDTO);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        }
    }

    @GetMapping(value="/{equipe}", produces = "application/json")
    public ResponseEntity<?> listarAtendimentos(@PathVariable EquipeSolicitacao equipe) {
        try {
            HashMap<Atendente, List<Solicitacao>> solicitacaoList = solicitacaoService.buscaSolicitacaoList(equipe);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(solicitacaoList);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage());
            return ResponseEntity.badRequest()
                    .body(e.getMessage());
        }
    }

}

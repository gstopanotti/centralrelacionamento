package com.invext.centralrelacionalmento.factory;


import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import com.invext.centralrelacionalmento.service.equipe.Equipe;
import com.invext.centralrelacionalmento.service.equipe.EquipeCartaoService;
import com.invext.centralrelacionalmento.service.equipe.EquipeEmprestimosService;
import com.invext.centralrelacionalmento.service.equipe.EquipeOutroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EquipeFactory {

    @Autowired
    EquipeCartaoService equipeCartaoService;

    @Autowired
    EquipeEmprestimosService equipeEmprestimosService;

    @Autowired
    EquipeOutroService equipeOutroService;


    public Equipe getAtendimentoByEquipe(EquipeSolicitacao equipeSolicitacao){

        switch (equipeSolicitacao) {
            case CARTOES:
                return equipeCartaoService;
            case EMPRESTIMOS:
                return equipeEmprestimosService;
            case OUTROS:
                return equipeOutroService;
            default:
                throw new RuntimeException("Equipe inválida");
        }
    }
}

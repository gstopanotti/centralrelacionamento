package com.invext.centralrelacionalmento.domain.model;

import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Atendente {

    private Long id;

    private String nome;

}

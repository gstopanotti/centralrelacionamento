package com.invext.centralrelacionalmento.domain.model;

import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
public class Solicitacao {

    UUID uuid = UUID.randomUUID();

    private String mensagem;

    private String cliente;

    private EquipeSolicitacao equipeSolicitacao;

    public Solicitacao(String mensagem, String cliente, EquipeSolicitacao equipeSolicitacao) {
        this.mensagem = mensagem;
        this.cliente = cliente;
        this.equipeSolicitacao = equipeSolicitacao;
    }
}

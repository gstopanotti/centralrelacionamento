package com.invext.centralrelacionalmento.domain.enums;

public enum EquipeSolicitacao  {

    CARTOES("Cartões"),
    EMPRESTIMOS("Empréstimos"),
    OUTROS("Outros assuntos");

    private String descricao;

    private EquipeSolicitacao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
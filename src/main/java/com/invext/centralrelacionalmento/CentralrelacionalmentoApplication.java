package com.invext.centralrelacionalmento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CentralrelacionalmentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CentralrelacionalmentoApplication.class, args);
	}

}

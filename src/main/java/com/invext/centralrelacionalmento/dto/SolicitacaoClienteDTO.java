package com.invext.centralrelacionalmento.dto;

import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import lombok.Data;

@Data
public class SolicitacaoClienteDTO {

    private String cliente;
    private String mensagem;
    private String equipeSolicitacao;


    public Solicitacao toDomain(){
        return new Solicitacao(this.cliente,  this.mensagem, EquipeSolicitacao.valueOf(equipeSolicitacao));
    }

}

package com.invext.centralrelacionalmento.service;


import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import com.invext.centralrelacionalmento.service.equipe.Equipe;
import com.invext.centralrelacionalmento.exception.AtendimentoNaoEncontradoException;
import com.invext.centralrelacionalmento.exception.NenhumAtendenteDisponivelException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class AtendimentoService {

    static Logger logger = Logger.getLogger(AtendimentoService.class.getName());

    static final int numeroMaximoAtendimentosPorAtendente = 3;

    HashMap<Atendente, List<Solicitacao>> atendimentoMap = new HashMap<>();


    public Atendente buscaAtendenteLivre(Equipe equipe) throws NenhumAtendenteDisponivelException {
        HashMap<Atendente, List<Solicitacao>> atendimentos = equipe.getAtendimentos();
        atendimentos.entrySet().stream().forEachOrdered(
                atendenteListEntry -> atendenteListEntry.getValue().size());
        for (Map.Entry<Atendente, List<Solicitacao>> atendente : atendimentos.entrySet()) {
            if (atendente.getValue().size() < numeroMaximoAtendimentosPorAtendente) {
                return atendente.getKey();
            }
        }
        throw new NenhumAtendenteDisponivelException("Sem atendentes disponíveis para equipe: " + equipe.getName());
    }

    public void encaminhaAtendimento(Equipe equipe, Solicitacao solicitacao) throws NenhumAtendenteDisponivelException {
        Atendente atendente = buscaAtendenteLivre(equipe);
        equipe.getAtendimentos().get(atendente).add(solicitacao);
        logger.info("Atendimento encaminhado:  " + solicitacao.getUuid() + " equipe: " + equipe.getName());
    }

    public boolean finalizaAtendimento(Equipe equipe, UUID uuid) throws AtendimentoNaoEncontradoException {

        for (Map.Entry<Atendente, List<Solicitacao>> atendente : equipe.getAtendimentos().entrySet()) {
            for (Solicitacao solicitacao : atendente.getValue()) {
                if (solicitacao.getUuid().equals(uuid)) {
                    atendente.getValue().remove(solicitacao);
                    return true;
                }
            }
        }
        throw new AtendimentoNaoEncontradoException("Atendimento não encontrado: " + uuid);
    }

    public HashMap<Atendente, List<Solicitacao>> listaAtendimentos(Equipe equipe) {
        return equipe.getAtendimentos();
    }
}

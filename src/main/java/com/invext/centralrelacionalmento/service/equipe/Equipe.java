package com.invext.centralrelacionalmento.service.equipe;

import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import com.invext.centralrelacionalmento.exception.NenhumAtendenteDisponivelException;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public interface Equipe {

    public HashMap<Atendente, List<Solicitacao>> registraAtendentes();

    public HashMap<Atendente, List<Solicitacao>> getAtendimentos();

    public String getName();

}

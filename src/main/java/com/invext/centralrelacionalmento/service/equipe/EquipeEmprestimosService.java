package com.invext.centralrelacionalmento.service.equipe;


import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class EquipeEmprestimosService implements Equipe {

    HashMap<Atendente, List<Solicitacao>> atendimentoMap = registraAtendentes();

    @Override
    public HashMap<Atendente, List<Solicitacao>> registraAtendentes() {
        HashMap<Atendente, List<Solicitacao>> atendimentos = new HashMap<>();
        atendimentos.put(new Atendente(5L, "Bruno"), new ArrayList<Solicitacao>());
        atendimentos.put(new Atendente(6L, "Camila"), new ArrayList<Solicitacao>());
        atendimentos.put(new Atendente(7L, "Luana"), new ArrayList<Solicitacao>());
        return atendimentos;

    }

    @Override
    public HashMap<Atendente, List<Solicitacao>> getAtendimentos() {
        return atendimentoMap;
    }

    @Override
    public String getName() {
        return "Empréstimos";
    }

}

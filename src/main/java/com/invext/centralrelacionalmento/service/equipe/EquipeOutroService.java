package com.invext.centralrelacionalmento.service.equipe;


import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class EquipeOutroService implements Equipe {

    HashMap<Atendente, List<Solicitacao>> atendimentoMap = registraAtendentes();

    @Override
    public HashMap<Atendente, List<Solicitacao>> registraAtendentes() {
        HashMap<Atendente, List<Solicitacao>> atendimentos = new HashMap<>();
        atendimentos.put(new Atendente(3L, "José"), new ArrayList<Solicitacao>());
        atendimentos.put(new Atendente(4L, "Roberta"), new ArrayList<Solicitacao>());
        return atendimentos;

    }

    @Override
    public HashMap<Atendente, List<Solicitacao>> getAtendimentos() {
        return atendimentoMap;
    }

    @Override
    public String getName() {
        return "Outros";
    }

}

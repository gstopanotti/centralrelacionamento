package com.invext.centralrelacionalmento.service.equipe;


import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class EquipeCartaoService implements Equipe {

    HashMap<Atendente, List<Solicitacao>> atendimentoMap = registraAtendentes();

    @Override
    public HashMap<Atendente, List<Solicitacao>> registraAtendentes() {
        HashMap<Atendente, List<Solicitacao>> atendimentos = new HashMap<>();
        atendimentos.put(new Atendente(1L, "Joao"), new ArrayList<Solicitacao>());
        atendimentos.put(new Atendente(2L, "Maria"), new ArrayList<Solicitacao>());
        return atendimentos;

    }

    @Override
    public HashMap<Atendente, List<Solicitacao>> getAtendimentos() {
        return atendimentoMap;
    }

    @Override
    public String getName() {
        return "Cartões";
    }

}

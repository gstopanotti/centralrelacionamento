package com.invext.centralrelacionalmento.service;


import com.invext.centralrelacionalmento.domain.enums.EquipeSolicitacao;
import com.invext.centralrelacionalmento.domain.model.Atendente;
import com.invext.centralrelacionalmento.domain.model.Solicitacao;
import com.invext.centralrelacionalmento.service.equipe.Equipe;
import com.invext.centralrelacionalmento.exception.AtendimentoNaoEncontradoException;
import com.invext.centralrelacionalmento.exception.NenhumAtendenteDisponivelException;
import com.invext.centralrelacionalmento.factory.EquipeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class SolicitacaoService {
    static Logger logger = Logger.getLogger(SolicitacaoService.class.getName());
    @Autowired    EquipeFactory equipeFactory;
    @Autowired    AtendimentoService atendimentoService;
    public List<Solicitacao> solicitacaoQueue = new ArrayList<>();


    public Solicitacao registrarSolicitacao(Solicitacao solicitacao) {
        solicitacaoQueue.add(solicitacao);
        logger.info("Solicitacao recebida com sucesso;");
        return solicitacao;
    }

    public HashMap<Atendente, List<Solicitacao>> buscaSolicitacaoList(EquipeSolicitacao equipeSolicitacao) {
        Equipe equipe = equipeFactory.getAtendimentoByEquipe(equipeSolicitacao);
        return atendimentoService.listaAtendimentos(equipe);
    }

    @Scheduled(fixedDelay = 5000)
    public void encaminhaSolicitacao() {
        logger.info("Busca solicitações...");
        if (solicitacaoQueue.isEmpty()) {
            logger.info("Lista de solicitações vazia");
            return;
        }
        ArrayList<Solicitacao> encaminhadas = new ArrayList<>();
        for (Solicitacao solicitacao : solicitacaoQueue) {
            Equipe equipe = equipeFactory.getAtendimentoByEquipe(solicitacao.getEquipeSolicitacao());
            try {
                atendimentoService.encaminhaAtendimento(equipe , solicitacao);
                encaminhadas.add(solicitacao);
                logger.info("Solicitacao: " + solicitacao.getUuid() + " encaminhada com sucesso.");
            } catch (NenhumAtendenteDisponivelException e) {
                logger.info(e.getMessage());
            }
        }
        solicitacaoQueue.removeAll(encaminhadas);
    }

    public void finalizarSolicitacao(UUID uuid, Solicitacao solicitacao) {
        logger.info("Finalizando solicitacao...");
        Equipe equipe = equipeFactory.getAtendimentoByEquipe(solicitacao.getEquipeSolicitacao());
        try {
            atendimentoService.finalizaAtendimento(equipe, uuid);
            logger.info("Solicitacao finalizada: "+ uuid);
        } catch (AtendimentoNaoEncontradoException e) {
            logger.info(e.getMessage());
        }
    }
}

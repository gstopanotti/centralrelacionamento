package com.invext.centralrelacionalmento.exception;

public class AtendimentoNaoEncontradoException extends Exception {

    private static final long serialVersionUID = 1L;

    public AtendimentoNaoEncontradoException(String message) {
        super(message);
    }

}

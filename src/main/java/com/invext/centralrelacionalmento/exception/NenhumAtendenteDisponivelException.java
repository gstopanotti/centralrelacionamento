package com.invext.centralrelacionalmento.exception;

public class NenhumAtendenteDisponivelException extends Exception {

    private static final long serialVersionUID = 1L;

    public NenhumAtendenteDisponivelException(String message) {
        super(message);
    }

}
